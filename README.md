# Docker & Kubernetes
## Section 4 - Networking: (Cross-)Container Communication



## About

**Course** : [Docker & Kubernetes: The Practical Guide [2023 Edition]](https://www.udemy.com/course/docker-kubernetes-the-practical-guide/)

**Instructor** : [Maximilian Schwarzmüller](https://www.udemy.com/user/maximilian-schwarzmuller/)
